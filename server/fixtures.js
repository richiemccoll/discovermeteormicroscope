if (Posts.find().count() === 0){
  Posts.insert({
    title: "Introducting Telescope",
    author: "Sacha Grief",
    url: "http://sachagrief.com"
  });

  Posts.insert({
    title: "Meteor",
    author: "Tom Coleman",
    url: "http://meteor.com"
  });

  Posts.insert({
    title: "Blog post",
    author: "Steve",
    url: "http://google.com"
  });
}
